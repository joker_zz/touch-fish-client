﻿using System.Collections.ObjectModel;
using FFmpeg.AutoGen;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using TouchFish.Media.Models;
using TouchFish.Media.Net;

namespace TouchFish.Media.Core;

public static class NAudioCapture
{
    public delegate void CaptureStatusChangedDelegate(bool status);

    public static event CaptureStatusChangedDelegate? CaptureStatusChanged;
    private static MMDevice? _currentCaptureDevice;
    private static WasapiLoopbackCapture? _loopbackCapture;
    private static bool _captureStatus;

    private static readonly Tcp _tcpClient;
    
    /// <summary>
    /// 音频编码器
    /// </summary>
    private static RecodeAudioData? _audioEncode;

    static NAudioCapture()
    {
        _tcpClient = new Tcp();
        FFmpegHelper.InitFfmpegPath();
    }

    public static void BeginCapture()
    {
        _currentCaptureDevice = WasapiLoopbackCapture.GetDefaultLoopbackCaptureDevice();
        _loopbackCapture = new WasapiLoopbackCapture(_currentCaptureDevice);
        _loopbackCapture.DataAvailable += LoopbackCaptureOnDataAvailable;
        _loopbackCapture.RecordingStopped += LoopbackCaptureOnRecordingStopped;

        var mixFormat = _loopbackCapture.WaveFormat; // _currentCaptureDevice.AudioClient.MixFormat;
        
        SampleInfo audioInfo = new(mixFormat.Channels,
            AVSampleFormat.AV_SAMPLE_FMT_FLT,
            mixFormat.SampleRate,
            mixFormat.AverageBytesPerSecond);
        
        _audioEncode = new RecodeAudioData(audioInfo,_tcpClient);
        _audioEncode.InitEncoder();

        _loopbackCapture.StartRecording();
        _audioEncode.BeginConverter();
    }

    public static void EndCapture()
    {
        if (_loopbackCapture?.CaptureState == CaptureState.Capturing)
            _loopbackCapture.StopRecording();
    }

    /// <summary>
    /// 录制结束
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void LoopbackCaptureOnRecordingStopped(object? sender, StoppedEventArgs e)
    {
        _captureStatus = false;
        CaptureStatusChanged?.Invoke(_captureStatus);
    }

    /// <summary>
    /// 接收到录制音频的数据
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void LoopbackCaptureOnDataAvailable(object? sender, WaveInEventArgs e)
    {
        if (!_captureStatus)
        {
            _captureStatus = true;
            CaptureStatusChanged?.Invoke(_captureStatus);
        }
        
        _audioEncode.AddWaveData(new (){ Data = e.Buffer,DataLength = e.BytesRecorded});
    }


    private static void Dispose()
    {
        if (_audioEncode != null)
        {
            _audioEncode.Dispose();
            _audioEncode = null;
        }

        if (_currentCaptureDevice != null)
        {
            _currentCaptureDevice.Dispose();
            _currentCaptureDevice = null;
        }

        if (_loopbackCapture != null)
        {
            _loopbackCapture.DataAvailable -= LoopbackCaptureOnDataAvailable;
            _loopbackCapture.RecordingStopped -= LoopbackCaptureOnRecordingStopped;
            if (_loopbackCapture.CaptureState == CaptureState.Capturing)
                _loopbackCapture.StopRecording();
            _loopbackCapture.Dispose();
            _loopbackCapture = null;
        }
    }
}