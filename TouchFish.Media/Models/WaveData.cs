﻿namespace TouchFish.Media.Models;

internal struct WaveData
{
    public byte[] Data { get; set; }
    public int DataLength { get; set; }
}