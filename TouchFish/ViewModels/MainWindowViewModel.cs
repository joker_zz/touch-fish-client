﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CommunityToolkit.Mvvm.Input;
using TouchFish.ViewModels.Base;

namespace TouchFish.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public string Greeting => "Welcome to Avalonia!";


    public ICommand TestCommand => new Lazy<RelayCommand>(
        new RelayCommand(PlayTest)).Value;


    private void PlayTest()
    {
        string path = @"
https://xy117x158x188x8xy.mcdn.bilivideo.cn:8082/v1/resource/1315714417-1-30280.m4s?agrr=0&build=0&buvid=827EA4CE-724B-AD9E-B545-2ECA9488B6CE37781infoc&bvc=vod&bw=26754&deadline=1720715594&e=ig8euxZM2rNcNbdlhoNvNC8BqJIzNbfqXBvEqxTEto8BTrNvN0GvT90W5JZMkX_YN0MvXg8gNEV4NC8xNEV4N03eN0B5tZlqNxTEto8BTrNvNeZVuJ10Kj_g2UB02J0mN0B5tZlqNCNEto8BTrNvNC7MTX502C8f2jmMQJ6mqF2fka1mqx6gqj0eN0B599M%3D&f=u_0_0&gen=playurlv2&logo=A0020000&mcdnid=50004933&mid=327616563&nbs=1&nettype=0&og=hw&oi=1885263811&orderid=0%2C3&os=mcdn&platform=pc&sign=86ce1f&traceid=trIbTrTFhQQvRU_0_e_N&uipk=5&uparams=e%2Cuipk%2Cnbs%2Cdeadline%2Cgen%2Cos%2Coi%2Ctrid%2Cmid%2Cplatform%2Cog&upsig=2b110a6a2fa811bacbc5461fed4a6707";

        TouchFish.Media.Core.NAudioPlay play = new();
        Task.Run(() => play.PlayAac(path));
    }
}