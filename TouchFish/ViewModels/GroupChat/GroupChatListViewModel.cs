using System.Collections.Generic;
using System.Collections.ObjectModel;
using TouchFish.Models.GroupChatModels;

namespace TouchFish.ViewModels.GroupChat;

public class GroupChatListViewModel : Base.ViewModelBase
{
    #region Properties

    private GroupChatListBdm _selectedGroupChat;
    /// <summary>
    /// 选中的群聊
    /// </summary>
    public GroupChatListBdm SelectedGroupChat
    {
        get => _selectedGroupChat;
        set => SetProperty(ref _selectedGroupChat, value);
    }

    /// <summary>
    /// 群聊列表
    /// </summary>
    public readonly IList<GroupChatListBdm> GroupChatListSource = new ObservableCollection<GroupChatListBdm>();

    #endregion

    public GroupChatListViewModel()
    {
        GroupChatListSource.Add(new ()
        {
            Icon = "https://img1.baidu.com/it/u=1581072608,402434150&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501",
            GroupChatId = 1000,
            Header = "你看你吗",
            Creater = "Joker1",
            Peoples = 22
        });
        GroupChatListSource.Add(new ()
        {
            Icon = "https://img95.699pic.com/xsj/0f/sz/vz.jpg%21/fw/700/watermark/url/L3hzai93YXRlcl9kZXRhaWwyLnBuZw/align/southeast",
            GroupChatId = 1001,
            Header = "火鸡味过吧",
            Creater = "Joker2",
            Peoples = 10
        });
        GroupChatListSource.Add(new ()
        {
            Icon = "https://trademark.zbjimg.com/pattern-prod/2016/image_45/21973170.jpg",
            GroupChatId = 1002,
            Header = "奶白的学子",
            Creater = "Joker3",
            Peoples = 102
        });
        GroupChatListSource.Add(new ()
        {
            Icon = "https://tm-image.tianyancha.com/tm/730b47c43b773cb7eabc5e02b7c0fcb4.jpg",
            GroupChatId = 1003,
            Header = "bbb",
            Creater = "Joker4",
            Peoples = 100
        });
    }
}