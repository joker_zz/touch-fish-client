using System;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.DependencyInjection;
using TouchFish.ViewModels.GroupChat;

namespace TouchFish.ViewModels;

public sealed class ViewModelLocator
{
    private IServiceProvider _serviceProvider;

    public void SetServiceProvider(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;


    public ViewModels.MainWindowViewModel MainVm
    {
        get
        {
            using var scope = _serviceProvider.CreateScope();
            return scope.ServiceProvider.GetRequiredService<ViewModels.MainWindowViewModel>();
        }
    }

    public ViewModels.GroupChat.GroupChatListViewModel GroupChatListVm
    {
        get
        {
            using var scope = _serviceProvider.CreateScope();
            return scope.ServiceProvider.GetRequiredService<ViewModels.GroupChat.GroupChatListViewModel>();
        }
    }
}