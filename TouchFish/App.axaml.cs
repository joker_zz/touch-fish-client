using System;
using System.Net.Http;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Data.Core;
using Avalonia.Data.Core.Plugins;
using Avalonia.Markup.Xaml;
using Microsoft.Extensions.DependencyInjection;
using TouchFish.ViewModels;
using TouchFish.Views;

namespace TouchFish;

public partial class App : Application
{

    private static IServiceProvider _currentProvider;

    public static T GetData<T>()
    {
        using var scope = _currentProvider.CreateScope();
        return scope.ServiceProvider.GetRequiredService<T>();
    }
    
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);

        IServiceCollection serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton<HttpClient>();
        serviceCollection.RegisterViewModel();
        var provider = serviceCollection.BuildServiceProvider();
        var vmLocator = this.Resources["VmLocator"] as ViewModels.ViewModelLocator;
        vmLocator?.SetServiceProvider(provider);

        _currentProvider = provider;
    }

    public override void OnFrameworkInitializationCompleted()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            // Line below is needed to remove Avalonia data validation.
            // Without this line you will get duplicate validations from both Avalonia and CT
            BindingPlugins.DataValidators.RemoveAt(0);
            desktop.MainWindow = new MainWindow();
        }

        base.OnFrameworkInitializationCompleted();
    }
    
    
}

static class RegisterProvider
{
    public static void RegisterViewModel(this IServiceCollection collection)
    {
        collection.AddSingleton<ViewModels.MainWindowViewModel>();
        collection.AddSingleton<ViewModels.GroupChat.GroupChatListViewModel>();
    }
}