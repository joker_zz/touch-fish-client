using CommunityToolkit.Mvvm.ComponentModel;

namespace TouchFish.Models.GroupChatModels;

public class GroupChatListBdm : ObservableObject
{
    public int GroupChatId { get; set; }

    private string _icon;

    /// <summary>
    /// 群聊头像
    /// </summary>
    public string Icon
    {
        get => this._icon;
        set => SetProperty(ref _icon, value);
    }

    private string _header;

    /// <summary>
    /// 群聊名称    
    /// </summary>
    public string Header
    {
        get => _header;
        set => SetProperty(ref _header, value);
    }

    private string _creater;

    /// <summary>
    ///     群里创建者
    /// </summary>
    public string Creater
    {
        get => _creater;
        set => SetProperty(ref _creater, value);
    }

    private int _peoples;

    /// <summary>
    /// 当前群聊人数
    /// </summary>
    public int Peoples
    {
        get => _peoples;
        set => SetProperty(ref _peoples, value);
    }
}